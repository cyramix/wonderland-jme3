This is a weekly build by Kyle Schwarz.

HawkEye's FFmpeg Builds Home Page: http://hawkeye.arrozcru.org

Built on Apr  2 2011 22:33:15

FFmpeg version git-f354396
	libavutil    50. 40. 0 / 50. 40. 0
	libavcodec   52.117. 0 / 52.117. 0
	libavformat  52.104. 0 / 52.104. 0
	libavdevice  52.  4. 0 / 52.  4. 0
	libavfilter   1. 76. 0 /  1. 76. 0
	libswscale    0. 13. 0 /  0. 13. 0

FFmpeg configured with:
	--disable-static
	--enable-shared
	--enable-gpl
	--enable-version3
	--arch=x86_64
	--target-os=mingw32
	--cross-prefix=x86_64-w64-mingw32-
	--enable-avisynth
	--enable-bzlib
	--enable-frei0r
	--enable-libopencore-amrnb
	--enable-libopencore-amrwb
	--enable-libfreetype
	--enable-libgsm
	--enable-libmp3lame
	--enable-libopenjpeg
	--enable-librtmp
	--enable-libschroedinger
	--enable-libspeex
	--enable-libtheora
	--enable-libvorbis
	--enable-libvpx
	--enable-libx264
	--enable-libxavs
	--enable-libxvid
	--enable-zlib
	--extra-cflags="-I/home/kyle/software/ffmpeg/external-libs/64-bit/include"
	--extra-ldflags="-L/home/kyle/software/ffmpeg/external-libs/64-bit/lib"
	--pkg-config=pkg-config

The source code for this FFmpeg build can be found at:
	http://hawkeye.arrozcru.org/source/
	
This version of FFmpeg was built on:
	Ubuntu Desktop 10.10: http://www.ubuntu.com/desktop
	
The cross-compile toolchain used to compile this FFmpeg was:
	MinGW-w64 r4102: http://mingw-w64.sourceforge.net/

The GCC version used to compile this FFmpeg was:
	GCC 4.5.2: http://gcc.gnu.org/
	
The external libaries compiled into this FFmpeg are:
	bzip2 1.0.6 http://www.bzip.org
	Frei0r 1.3 http://frei0r.dyne.org/
	opencore-amr 0.1.2 http://sourceforge.net/projects/opencore-amr/
	FreeType 2.4.4 http://www.freetype.org/
	gsm 1.0.13-3 http://libgsm.sourcearchive.com/
	LAME 3.98.4 http://lame.sourceforge.net/
	OpenJPEG 1.4 http://www.openjpeg.org/
	RTMP r565 http://rtmpdump.mplayerhq.hu/
	Schroedinger 1.0.10 http://diracvideo.org/
	Speex 1.2rc1 http://www.speex.org/
	Theora 1.1.1 http://www.theora.org/
	Vorbis 1.3.2 http://www.vorbis.com/
	libvpx 0.9.6 http://www.webmproject.org/code/
	x264 git-08d04a4d http://www.videolan.org/developers/x264.html
	XAVS r51 http://xavs.sourceforge.net/
	Xvid 1.3.1 http://www.xvid.org/
	zlib 1.2.5 http://zlib.net/
	
License for each library can be found in the "licenses" folder.