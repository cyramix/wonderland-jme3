/**
 * Open Wonderland
 *
 * Copyright (c) 2010, Open Wonderland Foundation, All Rights Reserved
 *
 * Redistributions in source code form must reproduce the above
 * copyright and this condition.
 *
 * The contents of this file are subject to the GNU General Public
 * License, Version 2 (the "License"); you may not use this file
 * except in compliance with the License. A copy of the License is
 * available at http://www.opensource.org/licenses/gpl-license.php.
 *
 * The Open Wonderland Foundation designates this particular file as
 * subject to the "Classpath" exception as provided by the Open Wonderland
 * Foundation in the License file that accompanied this code.
 */

package org.jdesktop.wonderland.client.jme3;

import com.jme3.asset.AssetManager;
import org.jdesktop.wonderland.client.ClientContext;

/**
 * A subclass of ClientContext which adds JME client specific context accessors.
 * 
 * @author paulby
 */
public class ClientContextJME extends ClientContext {

    private static WorldManager worldManager;

    private static JmeClientMain clientMain;
    private static AssetManager assetManager;

    static {
        
        System.setProperty("BafCacheDir", "file://"+getUserDirectory().getAbsolutePath());

    }

    /**
     * Get the JME client main object
     * @return the jme main client object
     */
    public static JmeClientMain getClientMain() {
        return clientMain;
    }

    /**
     * Set the JME client main object
     * @param clientMain the main object
     */
    static void setClientMain(JmeClientMain clientMain) {
        worldManager = new WorldManager();
        
        worldManager.setRootNode( clientMain.getRootNode());
        ClientContextJME.clientMain = clientMain;
        
        ClientContextJME.assetManager = clientMain.getAssetManager();
    }

    /**
     * @return the assetManager
     */
    public static AssetManager getAssetManager() {
        return assetManager;
    }

    /**
     * @return the worldManager
     */
    public static WorldManager getWorldManager() {
        return worldManager;
    }
}
